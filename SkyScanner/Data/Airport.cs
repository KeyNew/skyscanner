﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Airport
    {
        public int AirportId { get; set; }
        public string AirportName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public Airport(string airportName, double latitude, double longitude)
        {
            if (!(airportName == null))
            {
                AirportName = airportName;
                Latitude = latitude;
                Longitude = longitude;

            }
            else
            {
                throw new ArgumentException();
            }

        }
    }
}
