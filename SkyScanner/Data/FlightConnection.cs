﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace Data
{
    public class FlightConnection
    {
        public int ConnectionId { get; set; }
        public Plane ConnectionPlane { get; set; }
        public List<Airport> ConnectionAirports { get; set; }
        public double FlightTime { get; set; }
        public double TotalTime { get; set; }
        public double TotalFuelConsumption { get; set; }
        public double FuelConsumptionPerPassenger { get; set; }

        public FlightConnection(Plane connectionPlane, List<Airport> connectionAirports, double flightTime, double totalTime, double totalFuelConsumption, double fuelConsumptionPerPassenger)
        {
            if (!(connectionPlane == null || connectionAirports == null || connectionAirports.Count < 2))
            {
                ConnectionPlane = connectionPlane;
                ConnectionAirports = connectionAirports;
                FlightTime = flightTime;
                TotalTime = totalTime;
                TotalFuelConsumption = totalFuelConsumption;
                FuelConsumptionPerPassenger = fuelConsumptionPerPassenger;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
