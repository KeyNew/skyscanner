﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Plane
    {
        public int PlaneId { get; set; }
        public string PlaneName { get; set; }
        public double Velocity { get; set; }
        public double FuelUsage { get; set; }
        public int NumberOfSeats { get; set; }

        public Plane(string planeName, double velocity, double fuelUsage, int numberOfSeats)
        {
            if (!(planeName == null))
            {
                PlaneName = planeName;
                Velocity = velocity;
                FuelUsage = fuelUsage;
                NumberOfSeats = numberOfSeats;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }    
}
