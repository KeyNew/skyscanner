﻿using Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBAL
{
    public class SkyScannerContext : DbContext
    {
        public SkyScannerContext() : base()
        {
            
        }

        public DbSet<Airport> Airports { get; set; }
        public DbSet<Plane> Planes { get; set; }
        public DbSet<FlightConnection> FlightConnections { get; set; }
    }
}
