﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic
{
    public class Calculates
    {

        // Code taken (with small changes) from the website: https://www.geodatasource.com/developers/c-sharp
        public double DistanceCalculator(double lat1, double lon1, double lat2, double lon2)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double distance = Math.Sin(Deg2rad(lat1)) * Math.Sin(Deg2rad(lat2)) + Math.Cos(Deg2rad(lat1)) * Math.Cos(Deg2rad(lat2)) * Math.Cos(Deg2rad(theta));
                distance = Math.Acos(distance);
                distance = Rad2deg(distance);
                distance = distance * 60 * 1.1515 * 1.609344;                
                return (distance);
            }
        }

        public double TimeCalculator(double totalDistance, double velocity)
        {
            return totalDistance / velocity;
        }

        
        public double FuelConsumptionCalculator(double totalFlightTime, double fuelConsumption)
        {
            double totalFuelConsumption = totalFlightTime * fuelConsumption;
            return (totalFuelConsumption);
        }



        //This function converts decimal degrees to radians:
        private double Deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }
        //This function converts radians to decimal degrees
        private double Rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
    }
}
