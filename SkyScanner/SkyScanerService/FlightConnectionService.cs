﻿using BusinessLogic;
using Data;
using DBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyScanerService
{
    public class FlightConnectionService
    {
        public void AddConnection(Plane connectionPlane, List<Airport> connectionAirports, double flightTime, double totalTime, double totalFuelConsumption, double fuelConsumptionPerPassenger)
        {
            using (var _context = new SkyScannerContext())
            {
                var flightConnection = new FlightConnection(connectionPlane, connectionAirports, flightTime, totalTime, totalFuelConsumption, fuelConsumptionPerPassenger);
                _context.FlightConnections.Add(flightConnection);
                _context.SaveChanges();
            }
        }
        public List<FlightConnection> GetAvailableConnections()
        {
            using (var _context = new SkyScannerContext())
            {
                var connections = _context.FlightConnections.ToList();
                return connections;
            }
        }
        public void DeleteConnection(int id)
        {
            using (var _context = new SkyScannerContext())
            {                                
                _context.FlightConnections.Remove(_context.FlightConnections.FirstOrDefault(x => x.ConnectionId == id));
                _context.SaveChanges();
            }
        }
        public FlightConnection GetConnectionById(int id)
        {
            using (var _context = new SkyScannerContext())
            {
                var connection = _context.FlightConnections.FirstOrDefault(x => x.ConnectionId == id);
                return connection;
            }
        }
    //    public FlightConnection GetConnectionByShortestDistance(int airportId1, int airportId2)
    //    {
    //        using (var _context = new SkyScannerContext())
    //        {
    //            var connectionAirport1 = _context.Airports.FirstOrDefault(x => x.AirportId == airportId1);
    //            var connectionAirport2 = _context.Airports.FirstOrDefault(x => x.AirportId == airportId2);
    //            var connections = _context.FlightConnections.Where(x => x.ConnectionAirports.Contains(connectionAirport1)).Where(x => x.ConnectionAirports.Contains(connectionAirport2)).ToList();

    //            var properDirection = connections.Where(x => x.ConnectionAirports.IndexOf(connectionAirport1) < x.ConnectionAirports.IndexOf(connectionAirport2)).ToList();

    //            var calculates = new Calculates();
    //            double connectionDistance;
    //            double connectionTime;
    //            for (int i = 0; i < properDirection.Count; i++)
    //            {
    //                for (int j = properDirection[i].ConnectionAirports.IndexOf(connectionAirport1); j < properDirection[i].ConnectionAirports.IndexOf(connectionAirport1); j++)
    //                {

    //                }
    //            }
    //            FlightConnection connection = connections[0];
    //            for (int i = 1; i < connections.Count; i++)
    //            {
    //                if (totalTime > connections[i].TotalTime)
    //                {
    //                    totalTime = connections[i].TotalTime;
    //                    connection = connections[i];
    //                } 
    //            }
    //            return connection;
    //        }
    //    }
    }
}
