﻿using Data;
using DBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyScanerService
{
    public class PlaneService
    {
        public void AddPlane(string planeName, double velocity, double fuelUsage, int numberOfSeats)
        {
            using (var _context = new SkyScannerContext())
            {                
                var plane = new Plane(planeName, velocity, fuelUsage, numberOfSeats);
                _context.Planes.Add(plane);
                _context.SaveChanges();
            }
        }
        public List<Plane> GetAvailablePlanes()
        {
            using (var _context = new SkyScannerContext())
            {
                var planes = _context.Planes.ToList();
                return planes;
            }
        }

        public Plane GetPlanesById(int id)
        {
            using (var _context = new SkyScannerContext())
            {
                var plane = _context.Planes.FirstOrDefault(x => x.PlaneId == id);
                return plane;
            }
        }
    }
}
