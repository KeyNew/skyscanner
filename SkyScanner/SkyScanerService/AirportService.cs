﻿using Data;
using DBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkyScanerService
{
    public class AirportService
    {
        public void AddAirport(string airportName, double latitude, double longitude)
        {
            using (var _context = new SkyScannerContext())
            {
                var airport = new Airport(airportName, latitude, longitude);
                _context.Airports.Add(airport);
                _context.SaveChanges();
            }
        }
        public List<Airport> GetAvailableAirports()
        {
            using (var _context = new SkyScannerContext())
            {
                var airports = _context.Airports.ToList();
                return airports;
            }
        }
        public Airport GetAirportsById(int id)
        {
            using (var _context = new SkyScannerContext())
            {
                var airport = _context.Airports.FirstOrDefault(x => x.AirportId == id);
                return airport;
            }
        }
    }
}
