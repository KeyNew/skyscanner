﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SkyScanner.Cli.Menu
{
    public enum MainMenu
    {
        [Description("Dodaj samolot")]
        AddPlane = 1,

        [Description("Dodaj lotnisko")]
        AddAirport,

        [Description("Dodaj połączenie lotnicze")]
        AddFlightConnection,

        [Description("Modyfikuj połączenie lotnicze")]
        ModifyFlightConnection,

        [Description("Usuń połączenie")]
        DeleteFlightConnection,

        [Description("Szukaj połączeń")]
        SearchFlightConnection,

        Exit
    }
}
