﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SkyScanner.Cli.Menu
{
    class MenuService
    {
        private readonly Type _enumType;

        public MenuService(Type enumType)
        {
            if (!enumType.IsEnum)
            {
                throw new ArgumentException($"Input '{enumType}' is not enum");
            }
            _enumType = enumType;
        }

        public Enum AskUser()
        {
            foreach (var option in Enum.GetValues(_enumType))
            {
                var attribute = (DescriptionAttribute)Attribute.GetCustomAttribute(
                    option.GetType().GetField(option.ToString()),
                    typeof(DescriptionAttribute));

                Console.WriteLine($"{(int)option} - {attribute?.Description ?? option.ToString()}");
            }

            string userInput;
            int chosenOption;
            do
            {
                Console.WriteLine();
                Console.Write("Wybierz opcję: ");
                userInput = Console.ReadLine();
                Console.WriteLine();

            } while (!int.TryParse(userInput, out chosenOption) ||
                    !Enum.IsDefined(_enumType, chosenOption));

            return (Enum)Enum.ToObject(_enumType, chosenOption);
        }
    }
}
