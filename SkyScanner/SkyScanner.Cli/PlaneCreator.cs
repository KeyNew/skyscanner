﻿using Data;
using SkyScanerService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SkyScanner.Cli
{
    public class PlaneCreator
    {
        public PlaneCreator()
        {
            var planeName = GetPlaneName();
            var velocity = GetPlaneVelocity();
            var fuelUsage = GetFuelConsumption();
            var numberOfSeats = GetNumberOfSeats();
            var planeService = new PlaneService();
            planeService.AddPlane(planeName, velocity, fuelUsage, numberOfSeats);
        }
        public string GetPlaneName()
        {
            Console.WriteLine("Podaj nazwę samolotu: ");
            return Console.ReadLine();
        }
        public double GetPlaneVelocity()
        {
            Console.WriteLine("Podaj średnią prędkość przelotową samolotu [km/h]: ");
            double.TryParse(Console.ReadLine(), out double velocity);
            return velocity;
        }
        public double GetFuelConsumption()
        {
            Console.WriteLine("Podaj średnie spalanie samolotu [T/h]: ");
            double.TryParse(Console.ReadLine(), out double fuelConsumption);
            return fuelConsumption;
        }
        public int GetNumberOfSeats()
        {
            Console.WriteLine("Podaj ilość miejsc dla pasażerów: ");
            int.TryParse(Console.ReadLine(), out int numberOfSeats);
            return numberOfSeats;
        }        
    }
}
