﻿using Data;
using SkyScanerService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Schema;
using BusinessLogic;

namespace SkyScanner.Cli
{
    public class ConnectionCreator
    {
        public ConnectionCreator()
        {
            var planeService = new PlaneService();
            var availablePlanes = planeService.GetAvailablePlanes();
            foreach (var plane in availablePlanes)
            {
                Console.WriteLine($"Id: {plane.PlaneId}, Nazwa Samolotu: {plane.PlaneName} ");
            }
            Console.WriteLine("Wybierz samolot z listy wpisując jego Id: ");
            int.TryParse(Console.ReadLine(), out int planeId);
            var chosenPlane = planeService.GetPlanesById(planeId);
            
            var airportService = new AirportService();
            var availableAirports = airportService.GetAvailableAirports();
            var listOfAirports = new List<Airport>();
            foreach (var airport in availableAirports)
            {
                Console.WriteLine($"Id: {airport.AirportId}, Nazwa Lotniska: {airport.AirportName} ");
            }
            
            int counter = 1;
            string continueAnswer;
            do
            {                
                Console.WriteLine($"Wybierz {counter} lotnisko z listy wpisując jego Id: ");
                int.TryParse(Console.ReadLine(), out int airportId);
                var chosenAirport = airportService.GetAirportsById(airportId);
                listOfAirports.Add(chosenAirport);                
                counter++;
                Console.WriteLine("Jeśli chcesz dodać kolejne lotnisko wpisz 'Tak' ");
                continueAnswer = Console.ReadLine();
            } while (counter < 2 || continueAnswer == "Tak");

            double totalDistance = 0;
            var calculates = new Calculates(); 
            for (int i = 0; i < listOfAirports.Count; i++)
            {
                totalDistance = totalDistance + calculates.DistanceCalculator
                    (listOfAirports[i].Latitude, listOfAirports[i].Longitude, listOfAirports[i+1].Latitude, listOfAirports[i+1].Longitude);
            }
            double flightTime = calculates.TimeCalculator(totalDistance, chosenPlane.Velocity);
            double totalTime = flightTime + (double)listOfAirports.Count - 2;
            double totalFuelConsumption = flightTime * chosenPlane.FuelUsage;
            double fuelConsumptionPerPassenger = totalFuelConsumption / chosenPlane.NumberOfSeats;

            var connectionService = new FlightConnectionService();
            connectionService.AddConnection(chosenPlane, listOfAirports, flightTime, totalTime, totalFuelConsumption, fuelConsumptionPerPassenger);
        }
    }
}
