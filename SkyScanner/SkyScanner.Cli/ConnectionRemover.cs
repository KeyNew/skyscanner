﻿using SkyScanerService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SkyScanner.Cli
{
    public class ConnectionRemover
    {
        public ConnectionRemover()
        {
            var connectionService = new FlightConnectionService();
            var availableConnections = connectionService.GetAvailableConnections();
            foreach (var connection in availableConnections)
            {
                Console.Write($"Id: {connection.ConnectionId}, Trasa: ");
                foreach (var airport in connection.ConnectionAirports)
                {
                    Console.Write($"{airport}, ");
                }
            }
            Console.WriteLine("Wybierz połączenie z listy wpisując jego Id: ");
            int.TryParse(Console.ReadLine(), out int connectionId);
            var chosenConnection = connectionService.GetConnectionById(connectionId);
            connectionService.DeleteConnection(connectionId);
        }
    }
}
