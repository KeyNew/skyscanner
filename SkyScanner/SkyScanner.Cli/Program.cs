﻿using SkyScanner.Cli.Menu;
using System;

namespace SkyScanner.Cli
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {            
            MainMenu option;
            var menu = new MenuService(typeof(MainMenu));
            do
            {
                option = (MainMenu)menu.AskUser();
                switch (option)
                {
                    case MainMenu.AddPlane:
                        var planeCreator = new PlaneCreator();                        
                        break;
                    case MainMenu.AddAirport:
                        break;
                    case MainMenu.AddFlightConnection:
                        break;
                    case MainMenu.ModifyFlightConnection:
                        break;
                    case MainMenu.DeleteFlightConnection:
                        break;
                    case MainMenu.SearchFlightConnection:
                        break;
                    case MainMenu.Exit:
                        break;
                    default:
                        break;
                }
            } while (option != MainMenu.Exit);
        }
    }
}
