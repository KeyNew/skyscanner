﻿using Data;
using SkyScanerService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SkyScanner.Cli
{
    public class AirportCreator
    {
        
        public AirportCreator()
        {
            var airportName = GetAirportName();
            var latitude = GetLatitude();
            var longitude = GetLongitude();
            var airportService = new AirportService();
            airportService.AddAirport(airportName, latitude, longitude);
        }
        
        public string GetAirportName()
        {
            Console.WriteLine("Podaj nazwę lotniska: ");
            return Console.ReadLine();
        }
        public double GetLatitude()
        {
            Console.WriteLine("Podaj szerokość geograficzną [WGS84]: ");
            double.TryParse(Console.ReadLine(), out double latitude);
            return latitude;
        }
        public double GetLongitude()
        {
            Console.WriteLine("Podaj długość geograficzną[WGS84]: ");
            double.TryParse(Console.ReadLine(), out double longitude);
            return longitude;
        }
    }
}
